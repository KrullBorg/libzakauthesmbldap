/*
 * Copyright (C) 2010-2013 Andrea Zagli <azagli@libero.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
	#include "config.h"
#endif
  
#ifdef G_OS_WIN32
#include <windows.h>
#endif

#include <gtk/gtk.h>
#include <ldap.h>
#include <libssh/libssh.h>

#ifdef HAVE_LIBCONFI
	#include <libconfi.h>
#endif

#include "aute_smbldap.h"
#include "user.h"
#include "group.h"

static GtkBuilder *gtkbuilder;

static gchar *guifile;
static gchar *formdir;

static LDAP *ldap;
static gchar *base_dn;
static gchar *users_ou;
static gchar *groups_ou;

static gchar *host;
static gchar *host_ssh;
static gchar *utente;
static gchar *password;

static GtkWidget *txt_utente;
static GtkWidget *txt_password;
static GtkWidget *exp_cambio;
static GtkWidget *txt_password_nuova;
static GtkWidget *txt_password_conferma;

static GtkWidget *w_users;
static GtkWidget *notebook;

static GtkTreeView *trv_users;
static GtkListStore *lstore_users;
static GtkTreeSelection *sel_users;

static GtkTreeView *trv_groups;
static GtkListStore *lstore_groups;
static GtkTreeSelection *sel_groups;

static ssh_session session = NULL;
static ssh_channel channel = NULL;

enum
{
	COL_USERS_STATUS,
	COL_USERS_CN,
	COL_USERS_NAME,
	COL_USERS_UID
};

enum
{
	COL_GROUPS_CN,
	COL_GROUPS_GID
};

/* PRIVATE */
#ifdef G_OS_WIN32
static HMODULE backend_dll = NULL;

BOOL WINAPI DllMain (HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved);

BOOL WINAPI
DllMain (HINSTANCE hinstDLL,
         DWORD     fdwReason,
         LPVOID    lpReserved)
{
	switch (fdwReason)
		{
			case DLL_PROCESS_ATTACH:
				backend_dll = (HMODULE) hinstDLL;
				break;
			case DLL_THREAD_ATTACH:
			case DLL_THREAD_DETACH:
			case DLL_PROCESS_DETACH:
				break;
		}
	return TRUE;
}
#endif


#ifdef HAVE_LIBCONFI
static gboolean
get_connection_parameters_from_confi (Confi *confi, gchar **user_field)
{
	gboolean ret = TRUE;

	host = confi_path_get_value (confi, "aute/aute-smbldap/ldap/host");
	base_dn = confi_path_get_value (confi, "aute/aute-smbldap/ldap/base_dn");
	users_ou = confi_path_get_value (confi, "aute/aute-smbldap/ldap/users_ou");
	groups_ou = confi_path_get_value (confi, "aute/aute-smbldap/ldap/groups_ou");
	*user_field = confi_path_get_value (confi, "aute/aute-smbldap/ldap/user_field");

	if (host == NULL
	    || strcmp (g_strstrip (host), "") == 0
	    || base_dn == NULL
	    || strcmp (g_strstrip (base_dn), "") == 0
	    || users_ou == NULL
	    || strcmp (g_strstrip (users_ou), "") == 0
	    || groups_ou == NULL
	    || strcmp (g_strstrip (groups_ou), "") == 0
	    || user_field == NULL
	    || strcmp (g_strstrip (*user_field), "") == 0)
		{
			host = NULL;
			base_dn = NULL;
			users_ou = NULL;
			groups_ou = NULL;
			*user_field = NULL;

			ret = FALSE;
		}

	return ret;
}
#endif

static GtkWindow
*autesmbldap_get_gtkwidget_parent_gtkwindow (GtkWidget *widget)
{
	GtkWindow *w;
	GtkWidget *w_parent;

	w = NULL;

	w_parent = gtk_widget_get_parent (widget);
	while (w_parent != NULL && !GTK_IS_WINDOW (w_parent))
		{
			w_parent = gtk_widget_get_parent (w_parent);
		}

	if (GTK_IS_WINDOW (w_parent))
		{
			w = GTK_WINDOW (w_parent);
		}

	return w;
}

void
autesmbldap_exec_ssh_command (const gchar *command)
{
	int rc;
	char buf[4096];

	if (session == NULL)
		{
			session = ssh_new ();
			if (session == NULL)
				{
					g_warning ("Unable to open the SSH session.");
					return;
				}

			if (ssh_options_set (session, SSH_OPTIONS_USER, utente) < 0)
				{
					g_warning ("Unable to set the SSH user.");
					ssh_disconnect (session);
					session = NULL;
					return;
				}

			/*g_message("HOST SSH: %s",host_ssh);*/
			if (ssh_options_set (session, SSH_OPTIONS_HOST, host_ssh) < 0)
				{
					g_warning ("Unable to set the SSH host (%s).", host_ssh);
					ssh_disconnect (session);
					session = NULL;
					return;
				}
			if (ssh_connect (session))
				{
					g_warning ("SSH connection failed to host «%s» (%s).", host_ssh, ssh_get_error (session));
					ssh_disconnect (session);
					session = NULL;
					return;
				}

			rc = ssh_userauth_password (session, NULL, password);
			if (rc == SSH_AUTH_ERROR)
				{
					g_warning ("SSH authentication failed.");
					ssh_disconnect (session);
					session = NULL;
					return;
				}

			if (session == NULL)
				{
					g_warning ("SSH session null.");
					return;
				}
		}

	channel = channel_new (session);
	if (channel == NULL)
		{
			g_warning ("Unable to open the SSH channel.");
			ssh_disconnect (session);
			ssh_finalize ();
			return;
		}

	rc = channel_open_session (channel);
	if (rc < 0)
		{
			g_warning ("Unable to open the session on the SSH channel.");
			channel_close (channel);
			ssh_disconnect (session);
			ssh_finalize ();
			channel = NULL;
			return;
		}

	if (command != NULL)
		{
			gchar *cmd;

			cmd = g_strstrip (g_strdup (command));
			if (g_strcmp0 (cmd, "") == 0) return;

			/*g_message ("COMMAND: %s", cmd);*/
			rc = channel_request_exec (channel, cmd);
			if (rc < 0)
				{
					g_warning ("Unable to execute the SSH command: %s.", ssh_get_error (session));
					return;
				}

			do
				{
					if (channel_is_open (channel)
						&& !channel_is_eof (channel))
						{
							rc = channel_read (channel, buf, sizeof (buf), 1);
							if (rc > 0)
								{
									fwrite (buf, rc, 1, stdout);
								}
						}
				} while (rc > 0);
		}

	channel_close (channel);
	channel = NULL;
}

static void
autesmbldap_disconnect_ssh ()
{
	if (channel != NULL)
		{
			channel_send_eof (channel);
			channel_close (channel);
		}

	if (session != NULL)
		{
			ssh_disconnect (session);
			ssh_finalize ();
		}

	channel = NULL;
	session = NULL;
}

static gchar
*controllo (GSList *parameters)
{
	gchar *user_dn;
	gchar *password_nuova;

	gchar *user_field;

	gchar **parts;

	int version;
	int retldap;

	user_dn = "";

	utente = g_strstrip (g_strdup (gtk_entry_get_text (GTK_ENTRY (txt_utente))));
	password = g_strstrip (g_strdup (gtk_entry_get_text (GTK_ENTRY (txt_password))));

	host = NULL;
	base_dn = NULL;
	users_ou = NULL;
	groups_ou = NULL;
	user_field = NULL;

#ifdef HAVE_LIBCONFI
	/* the first and only parameters must be a Confi object */
	/* leggo i parametri di connessione dalla configurazione */
	if (IS_CONFI (parameters->data))
		{
			if (!get_connection_parameters_from_confi (CONFI (parameters->data), &user_field))
				{
					host = NULL;
					base_dn = NULL;
					users_ou = NULL;
					groups_ou = NULL;
					user_field = NULL;
				}
		}
#endif

	if (host == NULL)
		{
			GSList *param;

			param = g_slist_next (parameters);
			if (param != NULL && param->data != NULL)
				{
					host = g_strdup ((gchar *)param->data);
					host = g_strstrip (host);
					if (g_strcmp0 (host, "") == 0)
						{
							host = NULL;
						}
					else
						{
							param = g_slist_next (param);
							if (param != NULL && param->data != NULL)
								{
									base_dn = g_strdup ((gchar *)param->data);
									base_dn = g_strstrip (base_dn);
									if (g_strcmp0 (base_dn, "") == 0)
										{
											base_dn = NULL;
										}
									else
										{
											param = g_slist_next (param);
											if (param != NULL && param->data != NULL)
												{
													users_ou = g_strdup ((gchar *)param->data);
													users_ou = g_strstrip (users_ou);
													if (g_strcmp0 (users_ou, "") == 0)
														{
															users_ou = NULL;
														}
													else
														{
															param = g_slist_next (param);
															if (param != NULL && param->data != NULL)
																{
																	groups_ou = g_strdup ((gchar *)param->data);
																	groups_ou = g_strstrip (groups_ou);
																	if (g_strcmp0 (groups_ou, "") == 0)
																		{
																			groups_ou = NULL;
																		}
																	else
																		{
																			param = g_slist_next (param);
																			if (param != NULL && param->data != NULL)
																				{
																					user_field = g_strdup ((gchar *)param->data);
																					user_field = g_strstrip (user_field);
																					if (g_strcmp0 (user_field, "") == 0)
																						{
																							user_field = NULL;
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	if (host == NULL
		|| base_dn == NULL
		|| users_ou == NULL
		|| groups_ou == NULL
		|| user_field == NULL)
		{
			g_warning ("Missing some parameters.");
			return NULL;
		}

	parts = g_strsplit_set (host, "/:", -1);
	if (g_strv_length (parts) < 3)
		{
			g_warning ("Impossibile ottenere l'host per l'SSH.");
			host_ssh = NULL;
		}
	else
		{
			host_ssh = parts[3];
		}

	ldap = NULL;
	version = 3;

	retldap = ldap_initialize (&ldap, host);
	if (retldap != LDAP_SUCCESS)
		{
			g_warning ("Error on LDAP initialization: %s", ldap_err2string (retldap));
			return NULL;
		}

	retldap = ldap_set_option (ldap, LDAP_OPT_PROTOCOL_VERSION, &version);
	if (retldap != LDAP_OPT_SUCCESS)
		{
			g_warning ("Error on setting LDAP protocol version: %s", ldap_err2string (retldap));
			return NULL;
		}

	user_dn = g_strdup_printf ("%s=%s,%s,%s", user_field, utente, users_ou, base_dn);
	retldap = ldap_simple_bind_s (ldap, user_dn, password);
	if (retldap != LDAP_SUCCESS)
		{
			g_warning ("Error on LDAP binding: %s", ldap_err2string (retldap));
			return NULL;
		}

	/*retldap = ldap_unbind (ldap);
	if (retldap != LDAP_SUCCESS)
		{
			g_warning ("Errore nell'unbind: %s", ldap_err2string (retldap));
			return NULL;
		}*/

	if (g_strcmp0 (user_dn, "") != 0
	    && gtk_expander_get_expanded (GTK_EXPANDER (exp_cambio)))
		{
			password_nuova = g_strstrip (g_strdup (gtk_entry_get_text (GTK_ENTRY (txt_password_nuova))));
			if (strlen (password_nuova) == 0 || strcmp (g_strstrip (password_nuova), "") == 0)
				{
					/* TO DO */
					g_warning ("The new password is empty.");
				}
			else if (strcmp (g_strstrip (password_nuova), g_strstrip (g_strdup (gtk_entry_get_text (GTK_ENTRY (txt_password_conferma))))) != 0)
				{
					/* TO DO */
					g_warning ("The new password and the confirm don't match.");
				}
			else
				{
					/* cambio la password */
					gchar *command = g_strdup_printf ("echo -e \"%s\\n\" | sudo -k -S echo -e \"%s\\\\n%s\\\\n\" | sudo /usr/sbin/smbldap-passwd \"%s\"", password, password_nuova, password_nuova, utente);

					autesmbldap_exec_ssh_command (command);
				}
		}

	return user_dn;
}

static void
autesmbldap_load_users_list ()
{
	int retldap;
	LDAPMessage *result;
	LDAPMessage *entry;
	BerElement *ber;
	gchar *attr;
	struct berval **vals;

	guint i;

	gchar **attrs;

	GtkTreeIter iter;
	gchar *base;

	gtk_list_store_clear (lstore_users);

	attrs = g_strsplit ("cn|displayName|uidNumber", "|", -1);

	result = NULL;
	base = g_strdup_printf ("%s,%s", users_ou, base_dn);
	/*g_warning ("BASE USERS %s", base);*/
	retldap = ldap_search_ext_s (ldap, base, LDAP_SCOPE_ONELEVEL,
	                             NULL, attrs, 0, NULL, NULL, NULL,
	                             LDAP_NO_LIMIT, &result);
	if (retldap != LDAP_SUCCESS)
		{
			g_warning ("Errore nel search: %s", ldap_err2string (retldap));
			return;
		}
	entry = ldap_first_entry (ldap, result);
	while (entry != NULL)
		{
			gtk_list_store_append (lstore_users, &iter);

			for (attr = ldap_first_attribute (ldap, entry, &ber);
			     attr != NULL; attr = ldap_next_attribute (ldap, entry, ber))
				{
					vals = ldap_get_values_len (ldap, entry, attr);
					if (vals != NULL)
						{
							if (vals[0] != NULL)
								{
									if (g_strcmp0 (attr, "cn") == 0)
										{
											gtk_list_store_set (lstore_users, &iter,
													            COL_USERS_CN, vals[0]->bv_val,
													            -1);
										}
									else if (g_strcmp0 (attr, "displayName") == 0)
										{
											gtk_list_store_set (lstore_users, &iter,
													            COL_USERS_NAME, vals[0]->bv_val,
													            -1);
										}
									else if (g_strcmp0 (attr, "uidNumber") == 0)
										{
											if (strtol (vals[0]->bv_val, NULL, 10) < 1000)
												{
													/* system's user */
													/* undo the insertion */
													gtk_list_store_remove (lstore_users, &iter);
													break;
												}
											else
												{
													gtk_list_store_set (lstore_users, &iter,
																	    COL_USERS_UID, strtol (vals[0]->bv_val, NULL, 10),
																	    -1);
												}
										}
								}
							ldap_value_free_len (vals);
						}
					ldap_memfree (attr);
				}
			if (ber != NULL)
				{
					ber_free (ber, 0);
				}

			entry = ldap_next_entry (ldap, entry);	
		}
	ldap_msgfree (result);
}

static void
autesmbldap_load_groups_list ()
{
	int retldap;
	LDAPMessage *result;
	LDAPMessage *entry;
	BerElement *ber;
	gchar *attr;
	struct berval **vals;

	guint i;

	gchar **attrs;

	GtkTreeIter iter;
	gchar *base;

	gtk_list_store_clear (lstore_groups);

	attrs = g_strsplit ("cn|gidNumber", "|", -1);

	result = NULL;
	base = g_strdup_printf ("%s,%s", groups_ou, base_dn);
	/*g_warning ("BASE GROUPS %s", base);*/
	retldap = ldap_search_ext_s (ldap, base, LDAP_SCOPE_ONELEVEL,
	                             NULL, attrs, 0, NULL, NULL, LDAP_NO_LIMIT,
	                             LDAP_NO_LIMIT, &result);
	if (retldap != LDAP_SUCCESS)
		{
			g_warning ("Errore nel search: %s", ldap_err2string (retldap));
			return;
		}
	entry = ldap_first_entry (ldap, result);
	while (entry != NULL)
		{
			gtk_list_store_append (lstore_groups, &iter);

			for (attr = ldap_first_attribute (ldap, entry, &ber);
			     attr != NULL; attr = ldap_next_attribute (ldap, entry, ber))
				{
					vals = ldap_get_values_len (ldap, entry, attr);
					if (vals != NULL)
						{
							if (vals[0] != NULL)
								{
									if (g_strcmp0 (attr, "cn") == 0)
										{
											gtk_list_store_set (lstore_groups, &iter,
													            COL_GROUPS_CN, vals[0]->bv_val,
													            -1);
										}
									else if (g_strcmp0 (attr, "gidNumber") == 0)
										{
											if (strtol (vals[0]->bv_val, NULL, 10) < 1000)
												{
													/* system's group */
													/* undo the insertion */
													gtk_list_store_remove (lstore_groups, &iter);
													break;
												}
											else
												{
													gtk_list_store_set (lstore_groups, &iter,
																	    COL_GROUPS_GID, strtol (vals[0]->bv_val, NULL, 10),
																	    -1);
												}
										}
								}
							ldap_value_free_len (vals);
						}
					ldap_memfree (attr);
				}
			if (ber != NULL)
				{
					ber_free (ber, 0);
				}

			entry = ldap_next_entry (ldap, entry);	
		}
}

static void
autesmbldap_on_user_aggiornato (gpointer instance, gpointer user_data)
{
	autesmbldap_load_users_list ();
}

static void
autesmbldap_on_group_aggiornato (gpointer instance, gpointer user_data)
{
	autesmbldap_load_groups_list ();
}

static void
autesmbldap_edit_user ()
{
	GtkTreeIter iter;
	gchar *cn;

	GtkListStore *lstore;
	GtkTreeSelection *selection;
	gchar *str_type;
	guint col_cn;

	GtkWidget *w;
	GtkWidget *dialog;

	if (gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook)) == 0)
		{
			lstore = lstore_users;
			selection = sel_users;
			str_type = g_strdup ("user");
			col_cn = COL_USERS_CN;
		}
	else if (gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook)) == 1)
		{
			lstore = lstore_groups;
			selection = sel_groups;
			str_type = g_strdup ("group");
			col_cn = COL_GROUPS_CN;
		}
	else
		{
			dialog = gtk_message_dialog_new (autesmbldap_get_gtkwidget_parent_gtkwindow (w_users),
											 GTK_DIALOG_DESTROY_WITH_PARENT,
											 GTK_MESSAGE_WARNING,
											 GTK_BUTTONS_OK,
											 "Wrong notebook page.");
			return;
		}

	if (gtk_tree_selection_get_selected (selection, NULL, &iter))
		{
			gtk_tree_model_get (GTK_TREE_MODEL (lstore), &iter,
			                    col_cn, &cn,
								-1);

			if (gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook)) == 0)
				{
					User *u = user_new (gtkbuilder, guifile, ldap, cn, users_ou, base_dn);

					g_signal_connect (G_OBJECT (u), "aggiornato",
									  G_CALLBACK (autesmbldap_on_user_aggiornato), NULL);

					w = user_get_widget (u);
				}
			else if (gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook)) == 1)
				{
					Group *u = group_new (gtkbuilder, guifile, ldap, cn, groups_ou, base_dn);

					g_signal_connect (G_OBJECT (u), "aggiornato",
									  G_CALLBACK (autesmbldap_on_group_aggiornato), NULL);

					w = group_get_widget (u);
				}
			else
				{
					dialog = gtk_message_dialog_new (autesmbldap_get_gtkwidget_parent_gtkwindow (w_users),
													 GTK_DIALOG_DESTROY_WITH_PARENT,
													 GTK_MESSAGE_WARNING,
													 GTK_BUTTONS_OK,
													 "Wrong notebook page.");
					return;
				}

			gtk_window_set_transient_for (GTK_WINDOW (w), autesmbldap_get_gtkwidget_parent_gtkwindow (w_users));
			gtk_widget_show (w);
		}
	else
		{
			GtkWidget *dialog = gtk_message_dialog_new (autesmbldap_get_gtkwidget_parent_gtkwindow (w_users),
											 GTK_DIALOG_DESTROY_WITH_PARENT,
											 GTK_MESSAGE_WARNING,
											 GTK_BUTTONS_OK,
											 "You must select a %s before.", str_type);
			gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_destroy (dialog);
		}
}

static void
autesmbldap_on_btn_new_clicked (GtkButton *button,
                           gpointer user_data)
{
	GtkWidget *dialog;
	GtkWidget *w;

	if (gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook)) == 0)
		{
			User *u = user_new (gtkbuilder, guifile, ldap, "", users_ou, base_dn);

			g_signal_connect (G_OBJECT (u), "aggiornato",
					          G_CALLBACK (autesmbldap_on_user_aggiornato), NULL);

			w = user_get_widget (u);
		}
	else if (gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook)) == 1)
		{
			Group *u = group_new (gtkbuilder, guifile, ldap, "", groups_ou, base_dn);

			g_signal_connect (G_OBJECT (u), "aggiornato",
					          G_CALLBACK (autesmbldap_on_group_aggiornato), NULL);

			w = group_get_widget (u);
		}
	else
		{
			dialog = gtk_message_dialog_new (autesmbldap_get_gtkwidget_parent_gtkwindow (w_users),
											 GTK_DIALOG_DESTROY_WITH_PARENT,
											 GTK_MESSAGE_WARNING,
											 GTK_BUTTONS_OK,
											 "Wrong notebook page.");
			return;
		}

	gtk_window_set_transient_for (GTK_WINDOW (w), autesmbldap_get_gtkwidget_parent_gtkwindow (w_users));
	gtk_widget_show (w);
}
  
static void
autesmbldap_on_btn_edit_clicked (GtkButton *button,
                           gpointer user_data)
{
	autesmbldap_edit_user ();
}
  
static void
autesmbldap_on_btn_delete_clicked (GtkButton *button,
                           gpointer user_data)
{
	GtkWidget *dialog;
	gboolean risp;

	GtkTreeIter iter;
	gchar *code;

	GtkListStore *lstore;
	GtkTreeSelection *selection;
	gchar *str_type;
	static void (*load_list) (void);
	guint col_cn;
	gchar *command;

	if (gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook)) == 0)
		{
			lstore = lstore_users;
			selection = sel_users;
			str_type = g_strdup ("user");
			load_list = &autesmbldap_load_users_list;
			col_cn = COL_USERS_CN;
			command = g_strdup ("sudo /usr/sbin/smbldap-userdel -r ");
		}
	else if (gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook)) == 1)
		{
			lstore = lstore_groups;
			selection = sel_groups;
			str_type = g_strdup ("group");
			load_list = &autesmbldap_load_groups_list;
			col_cn = COL_GROUPS_CN;
			command = g_strdup ("sudo /usr/sbin/smbldap-groupdel ");
		}
	else
		{
			dialog = gtk_message_dialog_new (autesmbldap_get_gtkwidget_parent_gtkwindow (w_users),
											 GTK_DIALOG_DESTROY_WITH_PARENT,
											 GTK_MESSAGE_WARNING,
											 GTK_BUTTONS_OK,
											 "Wrong notebook page.");
			return;
		}

	if (gtk_tree_selection_get_selected (selection, NULL, &iter))
		{
			dialog = gtk_message_dialog_new (autesmbldap_get_gtkwidget_parent_gtkwindow (w_users),
											 GTK_DIALOG_DESTROY_WITH_PARENT,
											 GTK_MESSAGE_QUESTION,
											 GTK_BUTTONS_YES_NO,
											 "Are you sure to want to delete the selected %s?", str_type);
			risp = gtk_dialog_run (GTK_DIALOG (dialog));
			if (risp == GTK_RESPONSE_YES)
				{
					gchar *cn;
					gtk_tree_model_get (GTK_TREE_MODEL (lstore), &iter,
											col_cn, &cn,
											-1);

					autesmbldap_exec_ssh_command (g_strdup_printf ("%s%s", command, cn));

					(*load_list) ();
				}
			gtk_widget_destroy (dialog);
		}
	else
		{
			dialog = gtk_message_dialog_new (autesmbldap_get_gtkwidget_parent_gtkwindow (w_users),
											 GTK_DIALOG_DESTROY_WITH_PARENT,
											 GTK_MESSAGE_WARNING,
											 GTK_BUTTONS_OK,
											 "You must select a %s before.", str_type);
			gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_destroy (dialog);
		}
}

static void
autesmbldap_on_trv_users_row_activated (GtkTreeView *tree_view,
                                             GtkTreePath *tree_path,
											 GtkTreeViewColumn *column,
											 gpointer user_data)
{
	autesmbldap_edit_user ();
}

static void
autesmbldap_on_trv_groups_row_activated (GtkTreeView *tree_view,
                                             GtkTreePath *tree_path,
											 GtkTreeViewColumn *column,
											 gpointer user_data)
{
	autesmbldap_edit_user ();
}

static void
autesmbldap_on_btn_find_clicked (GtkButton *button,
                           gpointer user_data)
{
}
  
/* PUBLIC */
gchar
*autentica_get_password (GSList *parameters, gchar **password)
{
	GError *error;
	gchar *ret = NULL;

	error = NULL;

	gtkbuilder = gtk_builder_new ();

#ifdef G_OS_WIN32
	gchar *guidir;

	gchar *moddir;
	gchar *p;

	moddir = g_win32_get_package_installation_directory_of_module (backend_dll);

	p = g_strrstr (moddir, g_strdup_printf ("%c", G_DIR_SEPARATOR));
	if (p != NULL
	    && (g_ascii_strcasecmp (p + 1, "src") == 0
	    || g_ascii_strcasecmp (p + 1, ".libs") == 0))
		{
			guidir = g_strdup (GUIDIR);

#undef GUIDIR
		}
	else
		{
			guidir = g_build_filename (moddir, "share", PACKAGE, "gui", NULL);
		}

#endif

	guifile = g_build_filename (guidir, "autesmbldap.gui", NULL);
	if (!gtk_builder_add_objects_from_file (gtkbuilder, guifile,
	                                        g_strsplit ("diag_main", "|", -1),
	                                        &error))
		{
			g_warning ("Impossibile trovare il file di definizione dell'interfaccia utente: %s.",
			         error != NULL && error->message != NULL ? error->message : "no details");
			return NULL;
		}

	GtkWidget *diag = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "diag_main"));

	txt_utente = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "txt_utente"));
	txt_password = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "txt_password"));
	exp_cambio = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "exp_cambio"));
	txt_password_nuova = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "txt_password_nuova"));
	txt_password_conferma = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "txt_password_conferma"));

	/* imposto di default l'utente corrente della sessione */
	gtk_entry_set_text (GTK_ENTRY (txt_utente), g_get_user_name ());
	gtk_editable_select_region (GTK_EDITABLE (txt_utente), 0, -1);

#ifdef CANNOT_CHANGE_PASSWORD
	gtk_widget_set_no_show_all (exp_cambio, TRUE);
	gtk_widget_set_visible (exp_cambio, FALSE);
#endif

	switch (gtk_dialog_run (GTK_DIALOG (diag)))
		{
			case GTK_RESPONSE_OK:
				/* controllo dell'utente e della password */
				ret = controllo (parameters);
				break;

			case GTK_RESPONSE_CANCEL:
			case GTK_RESPONSE_NONE:
			case GTK_RESPONSE_DELETE_EVENT:
				ret = g_strdup ("");
				break;

			default:
				break;
		}

	if (ret != NULL && g_strcmp0 (ret, "") != 0 && password != NULL && *password == NULL)
		{
			if (g_strcmp0 (gtk_entry_get_text (GTK_ENTRY (txt_password_conferma)), "") != 0)
				{
					*password = g_strdup (gtk_entry_get_text (GTK_ENTRY (txt_password_conferma)));
				}
			else
				{
					*password = g_strdup (gtk_entry_get_text (GTK_ENTRY (txt_password)));
				}
		}

	gtk_widget_destroy (diag);
	g_object_unref (gtkbuilder);

	return ret;
}

gchar
*autentica (GSList *parameters)
{
	return autentica_get_password (parameters, NULL);
}

static void
autesmbldap_on_vbx_users_list_destroy (GtkWidget *object,
                            gpointer user_data)
{
	autesmbldap_disconnect_ssh ();
}

/**
 * get_management_gui:
 * @parameters:
 *
 */
GtkWidget
*get_management_gui (GSList *parameters)
{
	GError *error;

	error = NULL;

	gtkbuilder = gtk_builder_new ();

#ifdef G_OS_WIN32
#undef GUIDIR

	gchar *GUIDIR;

	GUIDIR = g_build_filename (g_win32_get_package_installation_directory_of_module (NULL), "share", "libaute-db", "gu", NULL);
#endif

	if (!gtk_builder_add_objects_from_file (gtkbuilder, g_build_filename (GUIDIR, "autesmbldap.gui", NULL),
	                                        g_strsplit ("lstore_users|lstore_groups|vbx_users_list", "|", -1),
	                                        &error))
		{
			g_error ("Impossibile trovare il file di definizione dell'interfaccia utente.");
			return NULL;
		}

	w_users = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "vbx_users_list"));
	if (w_users == NULL)
		{
			g_warning ("Unable to find the widget vbx_users_list.");
			return NULL;
		}

	g_signal_connect (gtk_builder_get_object (gtkbuilder, "vbx_users_list"),
	                  "destroy", G_CALLBACK (autesmbldap_on_vbx_users_list_destroy), NULL);

	notebook = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "notebook1"));

	trv_users = GTK_TREE_VIEW (gtk_builder_get_object (gtkbuilder, "treeview1"));
	lstore_users = GTK_LIST_STORE (gtk_builder_get_object (gtkbuilder, "lstore_users"));
	sel_users = gtk_tree_view_get_selection (trv_users);

	trv_groups = GTK_TREE_VIEW (gtk_builder_get_object (gtkbuilder, "treeview2"));
	lstore_groups = GTK_LIST_STORE (gtk_builder_get_object (gtkbuilder, "lstore_groups"));
	sel_groups = gtk_tree_view_get_selection (trv_groups);

	g_signal_connect (gtk_builder_get_object (gtkbuilder, "treeview1"),
	                  "row-activated", G_CALLBACK (autesmbldap_on_trv_users_row_activated), NULL);
	g_signal_connect (gtk_builder_get_object (gtkbuilder, "treeview2"),
	                  "row-activated", G_CALLBACK (autesmbldap_on_trv_groups_row_activated), NULL);

	g_signal_connect (G_OBJECT (gtk_builder_get_object (gtkbuilder, "button1")), "clicked",
	                  G_CALLBACK (autesmbldap_on_btn_new_clicked), NULL);
	g_signal_connect (G_OBJECT (gtk_builder_get_object (gtkbuilder, "button2")), "clicked",
	                  G_CALLBACK (autesmbldap_on_btn_edit_clicked), NULL);
	g_signal_connect (G_OBJECT (gtk_builder_get_object (gtkbuilder, "button3")), "clicked",
	                  G_CALLBACK (autesmbldap_on_btn_delete_clicked), NULL);
	g_signal_connect (G_OBJECT (gtk_builder_get_object (gtkbuilder, "button4")), "clicked",
	                  G_CALLBACK (autesmbldap_on_btn_find_clicked), NULL);

	autesmbldap_load_users_list ();
	autesmbldap_load_groups_list ();

	return w_users;
}
