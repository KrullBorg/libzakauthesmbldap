/* 
 * Copyright (C) 2010 Andrea Zagli <azagli@libero.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "user.h"
#include "aute_smbldap.h"

static void user_class_init (UserClass *klass);
static void user_init (User *user);

static void user_carica (User *user);
static void user_salva (User *user);

static void user_set_property (GObject *object,
                                     guint property_id,
                                     const GValue *value,
                                     GParamSpec *pspec);
static void user_get_property (GObject *object,
                                     guint property_id,
                                     GValue *value,
                                     GParamSpec *pspec);

static void user_on_btn_annulla_clicked (GtkButton *button,
                                    gpointer user_data);
static void user_on_btn_salva_clicked (GtkButton *button,
                                  gpointer user_data);

#define USER_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_USER, UserPrivate))

typedef struct _UserPrivate UserPrivate;
struct _UserPrivate
	{
		GtkBuilder *gtkbuilder;
		gchar *guifile;
		LDAP *ldap;

		GtkWidget *w;

		GtkWidget *txt_cn;
		GtkWidget *lbl_uid;
		GtkWidget *txt_surname;
		GtkWidget *txt_name;
		GtkWidget *chk_enabled;
		GtkWidget *txtv_description;

		gchar *cn;
		gchar *users_ou;
		gchar *base_dn;
	};

G_DEFINE_TYPE (User, user, G_TYPE_OBJECT)

static void
user_class_init (UserClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (object_class, sizeof (UserPrivate));

	object_class->set_property = user_set_property;
	object_class->get_property = user_get_property;

	/**
	 * User::aggiornato:
	 * @user:
	 *
	 */
	klass->aggiornato_signal_id = g_signal_new ("aggiornato",
	                                               G_TYPE_FROM_CLASS (object_class),
	                                               G_SIGNAL_RUN_LAST,
	                                               0,
	                                               NULL,
	                                               NULL,
	                                               g_cclosure_marshal_VOID__VOID,
	                                               G_TYPE_NONE,
	                                               0);
}

static void
user_init (User *user)
{
	UserPrivate *priv = USER_GET_PRIVATE (user);

	priv->cn = NULL;
	priv->users_ou = NULL;
	priv->base_dn = NULL;
}

/**
 * user_new:
 * @gtkbuilder:
 * @guifile:
 * @ldap:
 * @cn:
 * @users_ou:
 * @base_dn:
 *
 * Returns: the newly created #User object.
 */
User
*user_new (GtkBuilder *gtkbuilder, const gchar *guifile, LDAP *ldap,
            const gchar *cn, const gchar *users_ou, const gchar *base_dn)
{
	GError *error;

	g_return_val_if_fail (cn != NULL && users_ou != NULL && base_dn != NULL, NULL);

	User *a = USER (g_object_new (user_get_type (), NULL));

	UserPrivate *priv = USER_GET_PRIVATE (a);

	priv->gtkbuilder = gtkbuilder;
	priv->guifile = g_strdup (guifile);
	priv->ldap = ldap;

	error = NULL;
	gtk_builder_add_objects_from_file (priv->gtkbuilder, priv->guifile,
	                                   g_strsplit ("w_user", "|", -1),
	                                   &error);
	if (error != NULL)
		{
			g_warning ("Errore: %s.", error->message);
			return NULL;
		}

	priv->w = GTK_WIDGET (gtk_builder_get_object (priv->gtkbuilder, "w_user"));
	priv->txt_cn = GTK_WIDGET (gtk_builder_get_object (priv->gtkbuilder, "entry1"));
	priv->lbl_uid = GTK_WIDGET (gtk_builder_get_object (priv->gtkbuilder, "label19"));
	priv->txt_surname = GTK_WIDGET (gtk_builder_get_object (priv->gtkbuilder, "entry2"));
	priv->txt_name = GTK_WIDGET (gtk_builder_get_object (priv->gtkbuilder, "entry3"));
	priv->chk_enabled = GTK_WIDGET (gtk_builder_get_object (priv->gtkbuilder, "checkbutton1"));
	priv->txtv_description = GTK_WIDGET (gtk_builder_get_object (priv->gtkbuilder, "textview1"));

	g_signal_connect (gtk_builder_get_object (priv->gtkbuilder, "button5"),
	                  "clicked", G_CALLBACK (user_on_btn_annulla_clicked), (gpointer *)a);
	g_signal_connect (gtk_builder_get_object (priv->gtkbuilder, "button6"),
	                  "clicked", G_CALLBACK (user_on_btn_salva_clicked), (gpointer *)a);

	priv->cn = g_strstrip (g_strdup (cn));
	priv->users_ou = g_strstrip (g_strdup (users_ou));
	priv->base_dn = g_strstrip (g_strdup (base_dn));

	gtk_entry_set_text (GTK_ENTRY (priv->txt_cn), "");
	gtk_label_set_text (GTK_LABEL (priv->lbl_uid), "");
	if (g_strcmp0 (priv->cn, "") != 0)
		{
			user_carica (a);
		}

	return a;
}

/**
 * user_get_widget:
 * @user:
 *
 */
GtkWidget
*user_get_widget (User *user)
{
	UserPrivate *priv = USER_GET_PRIVATE (user);

	return priv->w;
}

/* PRIVATE */
static void
user_carica (User *user)
{
	UserPrivate *priv = USER_GET_PRIVATE (user);

	int retldap;
	LDAPMessage *result;
	LDAPMessage *entry;
	gchar **attrs;
	gchar *base;

	BerElement *ber;
	gchar *attr;
	struct berval **vals;

	attrs = g_strsplit ("cn|uidNumber", "|", -1);

	result = NULL;
	base = g_strdup_printf ("%s,%s", priv->users_ou, priv->base_dn);
	retldap = ldap_search_ext_s (priv->ldap, base, LDAP_SCOPE_ONELEVEL,
	                             g_strdup_printf ("(cn=%s)", priv->cn), attrs, 0, NULL, NULL, LDAP_NO_LIMIT,
	                             LDAP_NO_LIMIT, &result);
	if (retldap != LDAP_SUCCESS)
		{
			g_warning ("Errore nel search: %s", ldap_err2string (retldap));
			return;
		}

	entry = ldap_first_entry (priv->ldap, result);
	if (entry != NULL)
		{
			for (attr = ldap_first_attribute (priv->ldap, entry, &ber);
			     attr != NULL; attr = ldap_next_attribute (priv->ldap, entry, ber))
				{
					vals = ldap_get_values_len (priv->ldap, entry, attr);
					if (vals != NULL)
						{
							if (vals[0] != NULL)
								{
									if (g_strcmp0 (attr, "cn") == 0)
										{
											gtk_entry_set_text (GTK_ENTRY (priv->txt_cn), vals[0]->bv_val);
										}
									else if (g_strcmp0 (attr, "uidNumber") == 0)
										{
											gtk_label_set_text (GTK_LABEL (priv->lbl_uid), vals[0]->bv_val);
										}
								}
							ldap_value_free_len (vals);
						}
					ldap_memfree (attr);
				}
			if (ber != NULL)
				{
					ber_free (ber, 0);
				}
		}
	else
		{
			g_warning ("User «%s» not found.", g_strdup_printf ("cn=%s,%s", base, priv->cn));
		}
}

static void
user_salva (User *user)
{
	GtkWidget *dialog;

	UserClass *klass = USER_GET_CLASS (user);

	UserPrivate *priv = USER_GET_PRIVATE (user);

	if (g_strcmp0 (priv->cn, "") == 0)
		{
			/*autesmbldap_exec_ssh_command (g_strdup_printf ("sudo /usr/sbin/smbldap-useradd -a -G \"Domain Users\" -N \"$nome\" -S \"$cognome\" \"$username\" \"%s\"",
					          gtk_entry_get_text (GTK_ENTRY (priv->txt_cn))));
			priv->cn = g_strdup (gtk_entry_get_text (GTK_ENTRY (priv->txt_cn)));
			user_carica (user);*/
		}
	else
		{
			/*autesmbldap_exec_ssh_command (g_strdup_printf ("sudo /usr/sbin/smbldap-usermod -n \"%s\" \"%s\"",
					          gtk_entry_get_text (GTK_ENTRY (priv->txt_cn)), priv->cn));*/
		}

	/*if (gdaex_execute (priv->gdaex, sql) == 1)
		{*/
			g_signal_emit (user, klass->aggiornato_signal_id, 0);

			dialog = gtk_message_dialog_new (GTK_WINDOW (priv->w),
											 GTK_DIALOG_DESTROY_WITH_PARENT,
											 GTK_MESSAGE_INFO,
											 GTK_BUTTONS_OK,
											 "Salvataggio eseguito con successo.");
			gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_destroy (dialog);
		/*}
	else
		{
			dialog = gtk_message_dialog_new (GTK_WINDOW (priv->w),
											 GTK_DIALOG_DESTROY_WITH_PARENT,
											 GTK_MESSAGE_WARNING,
											 GTK_BUTTONS_OK,
											 "Errore durante il salvataggio.");
			gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_destroy (dialog);
		}*/
}

static void
user_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
	User *user = USER (object);

	UserPrivate *priv = USER_GET_PRIVATE (user);

	switch (property_id)
		{
			default:
				G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
				break;
		}
}

static void
user_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
	User *user = USER (object);

	UserPrivate *priv = USER_GET_PRIVATE (user);

	switch (property_id)
		{
			default:
				G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
				break;
		}
}

/* CALLBACK */
static void
user_on_btn_annulla_clicked (GtkButton *button,
                        gpointer user_data)
{
	User *user = (User *)user_data;

	UserPrivate *priv = USER_GET_PRIVATE (user);

	gtk_widget_destroy (priv->w);
}

static void
user_on_btn_salva_clicked (GtkButton *button,
                      gpointer user_data)
{
	user_salva ((User *)user_data);
}
