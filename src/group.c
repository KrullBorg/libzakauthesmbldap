/* 
 * Copyright (C) 2010 Andrea Zagli <azagli@libero.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "group.h"
#include "aute_smbldap.h"

static void group_class_init (GroupClass *klass);
static void group_init (Group *group);

static void group_carica (Group *group);
static void group_salva (Group *group);

static void group_set_property (GObject *object,
                                     guint property_id,
                                     const GValue *value,
                                     GParamSpec *pspec);
static void group_get_property (GObject *object,
                                     guint property_id,
                                     GValue *value,
                                     GParamSpec *pspec);

static void group_on_btn_annulla_clicked (GtkButton *button,
                                    gpointer group_data);
static void group_on_btn_salva_clicked (GtkButton *button,
                                  gpointer group_data);

#define GROUP_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_GROUP, GroupPrivate))

typedef struct _GroupPrivate GroupPrivate;
struct _GroupPrivate
	{
		GtkBuilder *gtkbuilder;
		gchar *guifile;
		LDAP *ldap;

		GtkWidget *w;

		GtkWidget *txt_cn;
		GtkWidget *lbl_gid;

		gchar *cn;
		gchar *groups_ou;
		gchar *base_dn;
	};

G_DEFINE_TYPE (Group, group, G_TYPE_OBJECT)

static void
group_class_init (GroupClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (object_class, sizeof (GroupPrivate));

	object_class->set_property = group_set_property;
	object_class->get_property = group_get_property;

	/**
	 * Group::aggiornato:
	 * @group:
	 *
	 */
	klass->aggiornato_signal_id = g_signal_new ("aggiornato",
	                                               G_TYPE_FROM_CLASS (object_class),
	                                               G_SIGNAL_RUN_LAST,
	                                               0,
	                                               NULL,
	                                               NULL,
	                                               g_cclosure_marshal_VOID__VOID,
	                                               G_TYPE_NONE,
	                                               0);
}

static void
group_init (Group *group)
{
	GroupPrivate *priv = GROUP_GET_PRIVATE (group);

	priv->cn = NULL;
	priv->groups_ou = NULL;
	priv->base_dn = NULL;
}

/**
 * group_new:
 * @gtkbuilder:
 * @guifile:
 * @ldap:
 * @cn:
 * @groups_ou:
 * @base_dn:
 *
 * Returns: the newly created #Group object.
 */
Group
*group_new (GtkBuilder *gtkbuilder, const gchar *guifile, LDAP *ldap,
            const gchar *cn, const gchar *groups_ou, const gchar *base_dn)
{
	GError *error;

	g_return_val_if_fail (cn != NULL && groups_ou != NULL && base_dn != NULL, NULL);

	Group *a = GROUP (g_object_new (group_get_type (), NULL));

	GroupPrivate *priv = GROUP_GET_PRIVATE (a);

	priv->gtkbuilder = gtkbuilder;
	priv->guifile = g_strdup (guifile);
	priv->ldap = ldap;

	error = NULL;
	gtk_builder_add_objects_from_file (priv->gtkbuilder, priv->guifile,
	                                   g_strsplit ("w_group", "|", -1),
	                                   &error);
	if (error != NULL)
		{
			g_warning ("Errore: %s.", error->message);
			return NULL;
		}

	priv->w = GTK_WIDGET (gtk_builder_get_object (priv->gtkbuilder, "w_group"));
	priv->txt_cn = GTK_WIDGET (gtk_builder_get_object (priv->gtkbuilder, "entry5"));
	priv->lbl_gid = GTK_WIDGET (gtk_builder_get_object (priv->gtkbuilder, "label18"));

	g_signal_connect (gtk_builder_get_object (priv->gtkbuilder, "button7"),
	                  "clicked", G_CALLBACK (group_on_btn_annulla_clicked), (gpointer *)a);
	g_signal_connect (gtk_builder_get_object (priv->gtkbuilder, "button8"),
	                  "clicked", G_CALLBACK (group_on_btn_salva_clicked), (gpointer *)a);

	priv->cn = g_strstrip (g_strdup (cn));
	priv->groups_ou = g_strstrip (g_strdup (groups_ou));
	priv->base_dn = g_strstrip (g_strdup (base_dn));

	gtk_entry_set_text (GTK_ENTRY (priv->txt_cn), "");
	gtk_label_set_text (GTK_LABEL (priv->lbl_gid), "");
	if (g_strcmp0 (priv->cn, "") != 0)
		{
			group_carica (a);
		}

	return a;
}

/**
 * group_get_widget:
 * @group:
 *
 */
GtkWidget
*group_get_widget (Group *group)
{
	GroupPrivate *priv = GROUP_GET_PRIVATE (group);

	return priv->w;
}

/* PRIVATE */
static void
group_carica (Group *group)
{
	GroupPrivate *priv = GROUP_GET_PRIVATE (group);

	int retldap;
	LDAPMessage *result;
	LDAPMessage *entry;
	gchar **attrs;
	gchar *base;

	BerElement *ber;
	gchar *attr;
	struct berval **vals;

	attrs = g_strsplit ("cn|gidNumber", "|", -1);

	result = NULL;
	base = g_strdup_printf ("%s,%s", priv->groups_ou, priv->base_dn);
	retldap = ldap_search_ext_s (priv->ldap, base, LDAP_SCOPE_ONELEVEL,
	                             g_strdup_printf ("(cn=%s)", priv->cn), attrs, 0, NULL, NULL, LDAP_NO_LIMIT,
	                             LDAP_NO_LIMIT, &result);
	if (retldap != LDAP_SUCCESS)
		{
			g_warning ("Errore nel search: %s", ldap_err2string (retldap));
			return;
		}

	entry = ldap_first_entry (priv->ldap, result);
	if (entry != NULL)
		{
			for (attr = ldap_first_attribute (priv->ldap, entry, &ber);
			     attr != NULL; attr = ldap_next_attribute (priv->ldap, entry, ber))
				{
					vals = ldap_get_values_len (priv->ldap, entry, attr);
					if (vals != NULL)
						{
							if (vals[0] != NULL)
								{
									if (g_strcmp0 (attr, "cn") == 0)
										{
											gtk_entry_set_text (GTK_ENTRY (priv->txt_cn), vals[0]->bv_val);
										}
									else if (g_strcmp0 (attr, "gidNumber") == 0)
										{
											gtk_label_set_text (GTK_LABEL (priv->lbl_gid), vals[0]->bv_val);
										}
								}
							ldap_value_free_len (vals);
						}
					ldap_memfree (attr);
				}
			if (ber != NULL)
				{
					ber_free (ber, 0);
				}
		}
	else
		{
			g_warning ("Group «%s» not found.", g_strdup_printf ("cn=%s,%s", base, priv->cn));
		}
}

static void
group_salva (Group *group)
{
	GtkWidget *dialog;

	GroupClass *klass = GROUP_GET_CLASS (group);

	GroupPrivate *priv = GROUP_GET_PRIVATE (group);

	if (g_strcmp0 (priv->cn, "") == 0)
		{
			autesmbldap_exec_ssh_command (g_strdup_printf ("sudo /usr/sbin/smbldap-groupadd -a \"%s\"",
					          gtk_entry_get_text (GTK_ENTRY (priv->txt_cn))));
			priv->cn = g_strdup (gtk_entry_get_text (GTK_ENTRY (priv->txt_cn)));
			group_carica (group);
		}
	else
		{
			autesmbldap_exec_ssh_command (g_strdup_printf ("sudo /usr/sbin/smbldap-groupmod -n \"%s\" \"%s\"",
					          gtk_entry_get_text (GTK_ENTRY (priv->txt_cn)), priv->cn));
		}

	/*if (gdaex_execute (priv->gdaex, sql) == 1)
		{*/
			g_signal_emit (group, klass->aggiornato_signal_id, 0);

			dialog = gtk_message_dialog_new (GTK_WINDOW (priv->w),
											 GTK_DIALOG_DESTROY_WITH_PARENT,
											 GTK_MESSAGE_INFO,
											 GTK_BUTTONS_OK,
											 "Salvataggio eseguito con successo.");
			gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_destroy (dialog);
		/*}
	else
		{
			dialog = gtk_message_dialog_new (GTK_WINDOW (priv->w),
											 GTK_DIALOG_DESTROY_WITH_PARENT,
											 GTK_MESSAGE_WARNING,
											 GTK_BUTTONS_OK,
											 "Errore durante il salvataggio.");
			gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_destroy (dialog);
		}*/
}

static void
group_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
	Group *group = GROUP (object);

	GroupPrivate *priv = GROUP_GET_PRIVATE (group);

	switch (property_id)
		{
			default:
				G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
				break;
		}
}

static void
group_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
	Group *group = GROUP (object);

	GroupPrivate *priv = GROUP_GET_PRIVATE (group);

	switch (property_id)
		{
			default:
				G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
				break;
		}
}

/* CALLBACK */
static void
group_on_btn_annulla_clicked (GtkButton *button,
                        gpointer group_data)
{
	Group *group = (Group *)group_data;

	GroupPrivate *priv = GROUP_GET_PRIVATE (group);

	gtk_widget_destroy (priv->w);
}

static void
group_on_btn_salva_clicked (GtkButton *button,
                      gpointer group_data)
{
	group_salva ((Group *)group_data);
}
