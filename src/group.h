/* 
 * Copyright (C) 2010 Andrea Zagli <azagli@libero.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#ifndef __GROUP_H__
#define __GROUP_H__

#include <glib.h>
#include <glib-object.h>

#include <gtk/gtk.h>
#include <ldap.h>

G_BEGIN_DECLS


#define TYPE_GROUP                 (group_get_type ())
#define GROUP(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GROUP, Group))
#define GROUP_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_GROUP, GroupClass))
#define IS_GROUP(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GROUP))
#define IS_GROUP_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_GROUP))
#define GROUP_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GROUP, GroupClass))


typedef struct _Group Group;
typedef struct _GroupClass GroupClass;

struct _Group
	{
		GObject parent;
	};

struct _GroupClass
	{
		GObjectClass parent_class;

		guint aggiornato_signal_id;
	};

GType group_get_type (void) G_GNUC_CONST;

Group *group_new (GtkBuilder *gtkbuilder, const gchar *guifile,
                  LDAP *ldap,
                  const gchar *cn, const gchar *groups_ou, const gchar *base_dn);

GtkWidget *group_get_widget (Group *group);


G_END_DECLS

#endif /* __GROUP_H__ */
